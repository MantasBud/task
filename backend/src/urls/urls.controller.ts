import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { UrlsService } from './urls.service';
import { CreateUrlDto } from './dto/create-url.dto';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';
import { GetUrlsDto } from './dto/get-urls-dto';

@Controller('urls')
export class UrlsController {
  constructor(private urlsService: UrlsService) {}

  @Get('')
  @UsePipes(ValidationPipe)
  @UseGuards(AuthGuard())
  getUrls(@GetUser() user: User, @Query() getUrlDto: GetUrlsDto) {
    return this.urlsService.getUrls(getUrlDto, user);
  }

  @Get(':tiny_url')
  getFullUrl(@Param('tiny_url') tiny_url: string) {
    return this.urlsService.getFullUrl(tiny_url);
  }

  @Post()
  @UsePipes(ValidationPipe)
  @UseGuards(AuthGuard())
  createUrl(@Body() createUrlDto: CreateUrlDto, @GetUser() user: User) {
    return this.urlsService.createUrl(createUrlDto, user);
  }

  @Delete(':id')
  @UseGuards(AuthGuard())
  deleteUrl(@Param('id') id: string, @GetUser() user: User) {
    return this.urlsService.deleteUrl(id, user);
  }
}
