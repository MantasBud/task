import { IsDefined, IsString } from 'class-validator';

export class GetUrlsDto {
  @IsDefined()
  @IsString()
  skip: string;

  @IsDefined()
  @IsString()
  take: string;
}
