import { IsUrl } from 'class-validator';

export class CreateUrlDto {
  @IsUrl()
  full_url: string;
}
