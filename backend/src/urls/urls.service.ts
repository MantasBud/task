import { Injectable, NotFoundException } from '@nestjs/common';
import { UrlRepository } from './url.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Url } from './url.entity';
import { CreateUrlDto } from './dto/create-url.dto';
import { GetUser } from '../auth/get-user.decorator';
import { User } from '../auth/user.entity';
import { GetUrlsDto } from './dto/get-urls-dto';

@Injectable()
export class UrlsService {
  constructor(
    @InjectRepository(UrlRepository) private urlRepository: UrlRepository,
  ) {}

  async getUrls(getUrlDto: GetUrlsDto, user: User): Promise<[Url[], number]> {
    const { skip, take } = getUrlDto;
    return await this.urlRepository.findAndCount({
      where: { 'created_by._id': user._id },
      skip: +skip,
      take: +take,
      order: {
        _id: 'DESC',
      },
    });
  }

  async getFullUrl(tiny_url: string): Promise<{ full_url: string }> {
    const url = await this.urlRepository.findOne({ tiny_url });
    if (!url) {
      throw new NotFoundException('Could not find tiny url');
    }
    return { full_url: url.full_url };
  }

  async createUrl(createUrlDto: CreateUrlDto, @GetUser() user: User) {
    return await this.urlRepository.createUrl(createUrlDto, user);
  }

  async deleteUrl(id: string, user: User) {
    return await this.urlRepository.deleteUrl(id, user);
  }
}
