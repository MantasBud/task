import { Module } from '@nestjs/common';
import { UrlsController } from './urls.controller';
import { UrlsService } from './urls.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UrlRepository } from './url.repository';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([UrlRepository]), AuthModule],
  controllers: [UrlsController],
  providers: [UrlsService],
})
export class UrlsModule {}
