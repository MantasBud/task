import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  ObjectID,
  ObjectIdColumn,
} from 'typeorm';
import { User } from '../auth/user.entity';

@Entity()
export class Url extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  tiny_url: string;

  @Column()
  full_url: string;

  @ManyToOne((type) => User, (user) => user.urls, { eager: false })
  created_by: User;
}
