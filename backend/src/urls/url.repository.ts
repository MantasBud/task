import { EntityRepository, Repository } from 'typeorm';
import { Url } from './url.entity';
import { CreateUrlDto } from './dto/create-url.dto';

import shortid = require('shortid');
import { User } from '../auth/user.entity';
import {
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { ObjectID } from 'mongodb';

@EntityRepository(Url)
export class UrlRepository extends Repository<Url> {
  async createUrl(createUrlDto: CreateUrlDto, user: User): Promise<Url> {
    const { full_url } = createUrlDto;

    const url = new Url();
    url.full_url = full_url;
    url.created_by = user;
    url.tiny_url = shortid.generate();
    await url.save();

    delete url.created_by;
    return url;
  }

  async deleteUrl(id: string, user: User) {
    const objectID: ObjectID = ObjectID.createFromHexString(id);
    const urlToDelete = await this.findOne({
      where: {
        'created_by._id': user._id,
        _id: objectID,
      },
    });
    if (!urlToDelete) {
      throw new NotFoundException(`Url with ID "${id}" not found`);
    }
    try {
      await this.delete(id);
    } catch {
      throw new InternalServerErrorException();
    }
    return { statusCode: 200, message: 'Successfully deleted tiny url' };
  }
}
