import { IsString, MaxLength, MinLength } from 'class-validator';
import { User } from '../user.entity';

export class UpdateUserCredentialsDto {
  user: User;

  @IsString()
  @MinLength(8)
  @MaxLength(20)
  password: string;
}
