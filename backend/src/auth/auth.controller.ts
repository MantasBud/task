import {
  Body,
  Controller,
  Patch,
  Post,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from './get-user.decorator';
import { User } from './user.entity';
import { UpdateUserCredentialsDto } from './dto/update-user-credentials-dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('signup')
  async signUp(@Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto) {
    await this.authService.signUp(authCredentialsDto);
    return this.signIn(authCredentialsDto);
  }

  @Post('signin')
  signIn(@Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto) {
    return this.authService.signIn(authCredentialsDto);
  }

  @Patch()
  @UseGuards(AuthGuard())
  async updateUser(@Body('password') password: string, @GetUser() user: User) {
    const updateUserCredentialsDto = new UpdateUserCredentialsDto();
    updateUserCredentialsDto.user = user;
    updateUserCredentialsDto.password = password;
    await this.authService.updateUser(updateUserCredentialsDto);

    const authCredentialsDto = new AuthCredentialsDto();
    authCredentialsDto.email = user.email;
    authCredentialsDto.password = password;
    return this.signIn(authCredentialsDto);
  }
}
