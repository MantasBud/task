import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { map, take } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.token.pipe(
      take(1),
      map(token => {
        const isAuth = !!token;
        if (isAuth) {
          if (route.routeConfig.path === 'signin' || route.routeConfig.path === 'signup' ) {
            return this.router.createUrlTree(['']);
          }
          return true;
        } else {
          if (route.routeConfig.path === 'signin' || route.routeConfig.path === 'signup' ) {
            return true;
          }
          return this.router.createUrlTree(['auth', 'signin']);
        }
    }));
  }
}
