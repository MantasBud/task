import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Token } from './token.model';
import { Router } from '@angular/router';

export interface AuthResponseData {
  accessToken: string;
  expiresIn: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token = new BehaviorSubject<Token>(null);
  private tokenExpirationTimer: any;

  constructor(private http: HttpClient, private router: Router) { }

  signup(email: string, password: string): Observable<AuthResponseData> {
    return this.http.post<AuthResponseData>('http://localhost:3000/auth/signup', {
      email,
      password
    }).pipe(
      catchError(this.handleError),
      tap(response => {
        this.handleAuthentication(response.accessToken, +response.expiresIn);
      })
    );
  }

  signIn(email: string, password: string): Observable<AuthResponseData> {
    return this.http.post<AuthResponseData>('http://localhost:3000/auth/signin', {
      email,
      password
    }).pipe(
      catchError(this.handleError),
      tap(response => {
        this.handleAuthentication(response.accessToken, +response.expiresIn);
      })
    );
  }

  autoLogin(): void {
    const tokenData: {
      token: string,
      tokenExpirationDate: string
    } = JSON.parse(localStorage.getItem('userToken'));
    if (!tokenData) {
      return;
    }
    const loadedToken = new Token(tokenData.token, new Date(tokenData.tokenExpirationDate));
    if (loadedToken.getToken()) {
      this.token.next(loadedToken);
      const expirationDuration = new Date(tokenData.tokenExpirationDate).getTime() - new Date().getTime();
      this.autoLogout(expirationDuration);
    }
  }

  logout(): void {
    this.token.next(null);
    this.router.navigate(['auth', 'signin']);
    localStorage.removeItem('userToken');
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
    this.tokenExpirationTimer = null;
  }

  autoLogout(expirationDuration: number): void {
    this.tokenExpirationTimer = setTimeout(() => {
      this.logout();
    }, expirationDuration);
  }

  changePassword(password: string): Observable<AuthResponseData> {
    return this.http.patch<AuthResponseData>('http://localhost:3000/auth', {
      password
    }).pipe(
      catchError(this.handleError),
      tap(response => {
        this.handleAuthentication(response.accessToken, +response.expiresIn);
      })
    );
  }

  private handleAuthentication(token: string, expiresIn: number): void {
    const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
    const newToken = new Token(token, expirationDate);
    this.token.next(newToken);
    this.autoLogout(expiresIn * 1000);
    localStorage.setItem('userToken', JSON.stringify(newToken));
  }

  private handleError(errorRes: HttpErrorResponse): Observable<any> {
    return throwError(errorRes);
  }
}
