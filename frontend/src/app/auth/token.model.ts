export class Token {
  constructor(
    private token: string,
    private tokenExpirationDate: Date
  ) {}

  getToken(): string {
    if (!this.tokenExpirationDate || new Date() > this.tokenExpirationDate) {
      return null;
    }
    return this.token;
  }
}
