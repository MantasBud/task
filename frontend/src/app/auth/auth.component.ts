import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthResponseData, AuthService } from './auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  isLoginMode;
  isLoading = false;
  error: string = null;
  authForm: FormGroup;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.router.url === '/auth/signin' ? this.isLoginMode = true : this.isLoginMode = false;

    this.authForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

  onAuthentication(): void {
    if (!this.authForm.valid) {
      return;
    }
    this.isLoading = true;
    const { email, password } = this.authForm.value;
    let authObs: Observable<AuthResponseData>;

    if (this.isLoginMode) {
      authObs = this.authService.signIn(email, password);
    } else {
      authObs = this.authService.signup(email, password);
    }

    authObs.subscribe(() => {
      this.isLoading = false;
      this.router.navigate(['/']);
    }, error => {
      this.error = error.error.message;
      this.isLoading = false;
    });
    this.authForm.reset();
  }

  getErrorMessageEmail(): string {
    if (this.authForm.get('email').hasError('required')) {
      return 'You must enter a value';
    } else if (this.authForm.get('email').hasError('email')){
      return 'Not a valid email';
    }
  }

  getErrorMessagePassword(): string {
    if (this.authForm.get('password').hasError('required')) {
      return 'You must enter a value';
    } else if (this.authForm.get('password').hasError('minlength')){
      return 'Password is too short';
    }
  }

  onHandleError(): void {
    this.error = null;
  }
}
