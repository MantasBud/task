import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  isLoading = false;
  error: string = null;
  changePwdForm: FormGroup;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.changePwdForm = new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      confirmPassword: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

  onPasswordChange(): void {
    if (!this.changePwdForm.valid) {
      return;
    }
    this.isLoading = true;
    const password = this.changePwdForm.get('password').value;

    this.authService.changePassword(password).subscribe(() => {
      this.isLoading = false;
      this.router.navigate(['/']);
    }, error => {
      this.error = error.error.message;
      this.isLoading = false;
    });
    this.changePwdForm.reset();
  }

  getErrorMessagePassword(): string {
    if (this.changePwdForm.get('password').hasError('required')) {
      return 'You must enter a value';
    } else if (this.changePwdForm.get('password').hasError('minlength')){
      return 'Password is too short';
    }
  }

  getErrorMessageConfirmPassword(): string {
    if (this.changePwdForm.get('password').value !== this.changePwdForm.get('confirmPassword').value) {
      return 'Passwords do not match';
    }
  }

  onHandleError(): void {
    this.error = null;
  }
}
