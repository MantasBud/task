import { Component, OnDestroy, OnInit } from '@angular/core';
import { Url } from '../url.interface';
import { UrlsService } from '../urls.service';
import { PageEvent } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-url-list',
  templateUrl: './url-list.component.html',
  styleUrls: ['./url-list.component.scss']
})
export class UrlListComponent implements OnInit, OnDestroy {
  urls: Url[];
  pageIndex = 0;
  length = 0;
  pageSize = 7;
  urlsSubscription: Subscription;
  urlsChangedSubscription: Subscription;

  get skip(): number {
    return this.pageIndex * this.pageSize;
  }

  constructor(private urlsService: UrlsService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.getUrls();

    this.urlsSubscription = this.urlsService.urls.subscribe((urls: [Url[], number]) => {
      this.urls = urls[0];
      this.length = urls[1];
    });

    this.urlsChangedSubscription = this.urlsService.urlsChanged.subscribe(() => {
      this.getUrls();
    });
  }

  ngOnDestroy(): void {
    this.urlsSubscription.unsubscribe();
    this.urlsChangedSubscription.unsubscribe();
  }

  onVisitUrl(tinyUrl: string): void {
    if (tinyUrl) {
      this.router.navigate([tinyUrl], { relativeTo: this.route });
    }
  }

  onDeleteUrl(url: Url): void {
    this.urlsService.deleteUrl(url._id).subscribe();
    if (this.skip >= this.length - 1 && this.pageIndex !== 0) {
      this.pageIndex--;
    }
  }

  getUrls(): void {
    this.urlsService.getUrls(this.skip, this.pageSize).subscribe();
  }

  updatePaginatorValues(event: PageEvent): void {
    this.pageIndex = event.pageIndex;
    this.getUrls();
  }

}
