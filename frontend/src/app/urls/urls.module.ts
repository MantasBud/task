import { NgModule } from '@angular/core';
import { UrlsComponent } from './urls.component';
import { UrlFormComponent } from './url-form/url-form.component';
import { UrlListComponent } from './url-list/url-list.component';
import { RedirectComponent } from './redirect/redirect.component';
import { UrlsRoutingModule } from './urls-routing.module';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    UrlsComponent,
    UrlFormComponent,
    UrlListComponent,
    RedirectComponent,
  ],
  imports: [
    SharedModule,
    UrlsRoutingModule,
    ClipboardModule,
  ]
})
export class UrlsModule { }
