export interface Url {
  _id: string;
  full_url: string;
  tiny_url: string;
}
