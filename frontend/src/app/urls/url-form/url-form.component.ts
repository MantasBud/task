import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UrlsService } from '../urls.service';
import { Url } from '../url.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { Clipboard } from '@angular/cdk/clipboard';

@Component({
  selector: 'app-url-form',
  templateUrl: './url-form.component.html',
  styleUrls: ['./url-form.component.scss']
})
export class UrlFormComponent implements OnInit {
  regex = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
  urlForm: FormGroup;
  tinyUrl = '';

  constructor(private urlsService: UrlsService, private router: Router, private route: ActivatedRoute, private clipboard: Clipboard) {}

  ngOnInit(): void {
    this.urlForm = new FormGroup({
      fullUrl: new FormControl('', [Validators.required, Validators.pattern(this.regex)])
    });
  }

  onCreateUrl(): void {
    if (!this.urlForm.valid) {
      return;
    }
    const fullUrl = this.urlForm.get('fullUrl').value;
    this.urlsService.createUrl(fullUrl).subscribe((url: Url) => {
      this.tinyUrl = url.tiny_url;
    });
  }

  getErrorMessageFullUrl(): string  {
    if (this.urlForm.hasError('required')) {
      return 'You must enter a value';
    } else {
      return 'Enter a valid url';
    }
  }

  onVisitUrl(): void {
    if (this.tinyUrl) {
      this.router.navigate([this.tinyUrl], { relativeTo: this.route });
    }
  }

  onCopyToClipboard(): void {
    if (this.tinyUrl) {
      this.clipboard.copy(`http://localhost:4200/${this.tinyUrl}`);
    }
  }
}
