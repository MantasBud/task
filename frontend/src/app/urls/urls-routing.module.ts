import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { UrlsComponent } from './urls.component';
import { RedirectComponent } from './redirect/redirect.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: UrlsComponent, canActivate: [AuthGuard], pathMatch: 'full'},
      { path: ':tiny_url', component: RedirectComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UrlsRoutingModule { }
