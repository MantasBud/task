import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subject, throwError } from 'rxjs';
import { Url } from './url.interface';

@Injectable({
  providedIn: 'root'
})
export class UrlsService {
  urls = new BehaviorSubject<[Url[], number]>([[], 0]);
  urlsChanged = new Subject();

  constructor(private http: HttpClient) {}

  createUrl(fullUrl: string): Observable<Url> {
    return this.http.post<Url>('http://localhost:3000/urls', {
      full_url: fullUrl
    }).pipe(
      catchError(this.handleError),
      tap(() => {
        this.urlsChanged.next(true);
      })
    );
  }

  getUrls(skip: number, take: number): Observable<[Url[], number]> {
    return this.http.get<[Url[], number]>(`http://localhost:3000/urls?skip=${skip}&take=${take}`).pipe(
      catchError(this.handleError),
      tap((resData => {
        this.urls.next(resData);
      }))
    );
  }

  deleteUrl(id: string): Observable<any> {
    return this.http.delete(`http://localhost:3000/urls/${id}`).pipe(
      catchError(this.handleError),
      tap(() => {
        this.urlsChanged.next(true);
      })
    );
  }

  redirectToFullUrl(tinyUrl: string): Observable<any> {
    return this.http.get<{ full_url: string }>(`http://localhost:3000/urls/${tinyUrl}`).pipe(
      catchError(this.handleError),
      tap((resData) => {
        const splitData = resData.full_url.split('//');
        if (splitData.length === 1) {
          window.location.href = 'https://' + resData.full_url;
        } else {
          window.location.href = resData.full_url;
        }
      })
    );
  }

  private handleError(errorRes: HttpErrorResponse): Observable<any> {
    return throwError(errorRes);
  }
}
