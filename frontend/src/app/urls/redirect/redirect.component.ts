import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { UrlsService } from '../urls.service';

@Component({
  selector: 'app-redirect',
  templateUrl: 'redirect.component.html'
})
export class RedirectComponent implements OnInit {
  tinyUrl: string;
  error: string = null;

  constructor(private router: Router, private route: ActivatedRoute, private urlsService: UrlsService) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.tinyUrl = params.tiny_url;
    });
    this.urlsService.redirectToFullUrl(this.tinyUrl).subscribe(() => {}, error => {
      this.error = error.error.message;
    });
  }

  onHandleError(): void {
    this.error = null;
    this.router.navigate(['']);
  }

}
